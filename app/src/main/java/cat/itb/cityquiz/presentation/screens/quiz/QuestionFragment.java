package cat.itb.cityquiz.presentation.screens.quiz;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.button.MaterialButton;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.Game;

public class QuestionFragment extends Fragment {

    private QuizViewModel mViewModel;
    ImageView imageView;
    private MaterialButton option1;
    private MaterialButton option2;
    private MaterialButton option3;
    private MaterialButton option4;
    private MaterialButton option5;
    private MaterialButton option6;

    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        // TODO: Use the ViewModel

        Game game = mViewModel.getGame();
        displayGame(game);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imageView = view.findViewById(R.id.imageView);
        option1 = view.findViewById(R.id.btnOption1);
        option2 = view.findViewById(R.id.btnOption2);
        option3 = view.findViewById(R.id.btnOption3);
        option4 = view.findViewById(R.id.btnOption4);
        option5 = view.findViewById(R.id.btnOption5);
        option6 = view.findViewById(R.id.btnOption6);

    }

    private void displayGame(Game game){
        String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        imageView.setImageResource(resId);

        configureAnswerButton(game, option1, 0);
        configureAnswerButton(game, option2, 1);
        configureAnswerButton(game, option3, 2);
        configureAnswerButton(game, option4, 3);
        configureAnswerButton(game, option5, 4);
        configureAnswerButton(game, option6, 5);
    }

    private void configureAnswerButton(Game game, MaterialButton button, int tag) {
        button.setText(game.getCurrentQuestion().getPossibleCities().get(tag).getName());
        button.setTag(tag);
        button.setOnClickListener(this::answerQuestion);
    }

    private void answerQuestion(View view) {
        int answerNumber = (Integer) view.getTag();
        Game game = mViewModel.answerQuestion(answerNumber);
        if(game.isFinished()) Navigation.findNavController(view).navigate(R.id.action_end_game);
        else displayGame(game);

    }
}
